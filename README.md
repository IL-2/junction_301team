# antispam301 team

Problem: Spam campaigns detection
Idea: Process a message body to detect suspicious mail
Technology:

1. Analyze url's reputation

2. Recognise common html patterns in different messages

3. Compute local sensitive hashing to group similar messages

** Input **: Path to a file

** Output **: signature - all non-None signatures are spam

Example:
```
from model import *

# Clusters for the hash
dirs = []
pwd = '../../data/honeypot-content/'
for f in os.listdir(pwd):
    full_name = pwd + f
    if not os.path.isfile(full_name):
        dirs.append(full_name)

a = Analyzer()
a.set_clusters([[i + '/' + j for j in os.listdir(i)] for i in dirs], [i.split('/')[-1] for i in dirs])

full_name = pwd + '1568160527.M188881P28206.mail1-fi1.d-fence.eu,S=2202,W=2242'
a.get_signature(full_name)
```