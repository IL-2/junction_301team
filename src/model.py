import os
import re
import email
import base64
from urlextract import URLExtract
import binascii
import tldextract
import numpy as np
import nilsimsa
from scipy.spatial.distance import cosine
from tqdm import tqdm
from bs4 import BeautifulSoup

def l1_norm(x):
    return np.sum(np.abs(x), axis=-1)

def l2_norm(x):
    return np.sum(x ** 2, axis=-1)

def dist(a, b, type_dist='l1'):
    if type_dist == 'l1':
        return l1_norm(a - b)
    elif type_dist == 'l2':
        return l2_norm(a - b)
    elif type_dist == 'cosine':
        return cosine(a, b)
    else:
        raise "Uknown type"

class eml_parser:
    def __init__(self, file):
        self.color_patterns = [
            'color',
            'color\s*:\s*#fff',
            'color\s*:\s*white'
        ]
        self.headers = None
        self.rule_porn_regexps = [ 'hotevelyn\.su|\
                                    bethany\.su|\
                                    \.su/unsub/unsub\.php|\
                                    sweetlaura\.su|\
                                    sexyna\.org|\
                                    Haluatko seksi tn iltana ja uutta pillua joka piv',
                                    "((</?(tr|td|tbody)+>)+)",
                                    '<img'
                                 ]
        self.reputation_regexps = [
            '(bit\.do/\w{5})',
            'www\.you\d+\.com/[a-z0-9]{32}_[a-z0-9]{8}-[0-9]{11}',
            '<style><[A-Z]{50}></style><center>[^<>]*<br><a href="[\w./:]+.html">CLICK HERE</a><br></center><style><[A-Z]{50}></style>',
            
        ]
        
        self.file = file
        
        try:
            self.msg = email.message_from_file(open(file, 'r'))
        except UnicodeDecodeError:
            self.msg = email.message_from_file(open(file, 'r', encoding="ISO-8859-1"))
        self.extractor = URLExtract()
        
    def get_bodies(self):
        bodies = []
        if self.msg.is_multipart():
            for part in self.msg.get_payload():
                
                tmp_part = part.get_payload()
                if isinstance(tmp_part, list):
                    bodies.extend(tmp_part)
                else:
                    bodies.append(tmp_part)
        else:
            bodies += [self.msg.get_payload()]
        return [str(body) for body in bodies]
    
    
    def base64(self, text):
        text2 = ''.join(text.split('\n'))
        pattern = '^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$'
        return True if len(re.findall(pattern, text2)) > 0 else 0
    
    def hash2html(self, s):
        return base64.decodebytes(bytes(s, 'utf-8')).decode('utf-8', errors='ignore')    
    
    def get_eml_id(self):
        return self.file.split('/')[-1]
    
    def get_html(self):
        
        htmls = []
        bodies = self.get_bodies()
        for body in bodies:
            if self.base64(body):
                text = self.hash2html(body)
                htmls += [text]
            else:
                htmls += [body]
        
        return htmls
    
    def compute_hash(self):
        if self.headers is None:
            self.get_headers()
            
        text = ' '.join(self.headers['html'])
        return np.array(nilsimsa.Nilsimsa(text).digest)
    
    def get_headers(self):
        self.headers = {}
        
        self.headers['eml_id'] = self.get_eml_id()
        self.headers['html'] = self.get_html()
        return self.headers
    
    def rule_color_white(self):
        if self.headers is None:
            self.get_headers()
            
        count_white = 0
        count_color = 0
        for html in self.headers['html']:
            html = html.lower()
            count_color += len(re.findall(self.color_patterns[0], html))
            count_white += len(re.findall(self.color_patterns[1], html) + \
                               re.findall(self.color_patterns[2], html))
        if count_white > (count_color - count_white) and count_white != 0:
            return True
        return False
    
    def rule_invisible_text(self):
        if self.headers is None:
            self.get_headers()
                    
        for html in self.headers['html']:
            #add rule to ban white text

            bs = BeautifulSoup(html)
            tags = bs.find_all(attrs={'style' : re.compile('font-size:\s*\d[^\d]')})
            for tag in tags:
                if len(tag.text) > 20:
                    return True
        return False
    
    def _empty_tags(self, matches, min_num_tags=4, min_num_empty_tags=2):
        count = 0
        for match in matches:
            if len(match.split('><')) > min_num_tags:
                count += 1
        return count > min_num_empty_tags
    
    def rule_porn(self, min_num_tags=4, min_num_empty_tags=2):
        if self.headers is None:
            self.get_headers()
        
        for html in self.headers['html']:
            if re.search(self.rule_porn_regexps[0], html):
                return True
            matches = re.findall(self.rule_porn_regexps[1], html)
            if len(re.findall(self.rule_porn_regexps[2], html)) == 4 \
            and self._empty_tags([i[0] for i in matches], 
                                 min_num_tags, 
                                 min_num_empty_tags):
                return True
        return False
    
    def rule_reputation(self):
        if self.headers is None:
            self.get_headers()
        
        bad_regexps = []
        for regex in self.reputation_regexps:
            for html in self.headers['html']:
                if re.search(regex, html):
                    bad_regexps.append(regex)
                    break
                    
        if len(bad_regexps):
            return bad_regexps
        return False
    
class Analyzer():
    def __init__(self, threshold_dist=1200):
        self.clusters = None
        
    def get_signature(self, file, type_dist='l1',
                     min_num_tags=4, min_num_empty_tags=2):
        self.eml_parser = eml_parser(file)
        
        is_porn = self.eml_parser.rule_porn(min_num_tags, min_num_empty_tags)
        if is_porn:
            return 'Porn'
        
        reputation = self.eml_parser.rule_reputation()
        if reputation:
            return 'reputation-' + str(hash(reputation[0]))
        
        is_invis = self.eml_parser.rule_invisible_text()
        if is_invis:
            return 'Invisible text'
        
        is_white = self.eml_parser.rule_color_white()
        if is_white:
            return 'White-color text'
        
        argmin, dists = self._find_closest_cluster(type_dist)
        if dists[argmin] <= threshold_dist:
            return self.clusters_names[argmin]
        
        return 'None'
        
#         return cluster_name, is_invis, is_porn, is_white, reputation
        
    def set_clusters(self, clusters, clusters_names):
        self.clusters_names = clusters_names
        self.clusters = np.array([
            np.mean([eml_parser(file).compute_hash() for file in cluster], axis=0)
            for cluster in tqdm(clusters)
        ])
                
    def _find_closest_cluster(self, type_dist='l1'):
        if self.clusters is None:
            raise "Clusters are not set"
        eml_hash = self.eml_parser.compute_hash()
        dists = [dist(c, eml_hash) for c in self.clusters]
        argmin = np.argmin(dists)
        return argmin, dists
