from model import *

dirs = []
pwd = '../../data/honeypot-content/'
for f in os.listdir(pwd):
    full_name = pwd + f
    if not os.path.isfile(full_name):
        dirs.append(full_name)

a = Analyzer()
a.set_clusters([[i + '/' + j for j in os.listdir(i)] for i in dirs], [i.split('/')[-1] for i in dirs])
results = []
for f in tqdm(os.listdir(pwd)):
    full_name = pwd + f
    if os.path.isfile(full_name):
        results.append((full_name, a.get_signature(full_name)))
